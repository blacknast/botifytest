import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

//import css
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";
import css from "./styles/styles.css";

// immport component
import Main from "./main";
import Todolist from "./component/footer.js";
import Header from "./component/header.js";




ReactDOM.render(<Main />, document.getElementById('root'));
registerServiceWorker();
