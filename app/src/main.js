import React from "react";
import Todolist from "./component/footer";
import Header   from "./component/header"

//import css
import css from "./styles/styles.css";

const Main = React.createClass({
        render() {
                return (
                        <div >
                                <Header />
                                <Todolist />
                        </div>
                );
        }
});

export default Main;
