import React from "react";

//import css
import css from "../styles/styles.css";

const TodoItems = React.createClass({
        render: function() {
                var todoEntries = this.props.entries;

                function createTasks(item) {
                        return <li key={item.key}>{item.text}</li>;
                }
                var listItems = todoEntries.map(createTasks);

                return (
                        <ul className="theList">
                                {listItems}
                        </ul>
                );
        }
});

const Todolist = React.createClass({
        getInitialState: function() {
                return {
                        items: JSON.parse(localStorage.state || "[]")
                };
        },

        addItem: function(e) {
                var itemArray = this.state.items;

                itemArray.push({
                        text: this._inputElement.value,
                        key: Date.now()
                });

                this.setState({
                        items: itemArray
                });

                this._inputElement.value = "";

                e.preventDefault();
                localStorage.state = JSON.stringify(itemArray);
        },

        render: function() {
                return (
                        <div>
                                <div className="todoListMain theList li">
                                        <TodoItems entries={this.state.items} />
                                </div>
                                <form onSubmit={this.addItem}>
                                        <textarea
                                                className="textarea-com"
                                                ref={a =>
                                                        (this._inputElement = a)}
                                                placeholder="add your comment"
                                        />
                                        <button type="submit" className="btn">
                                        Send
                                        </button>
                                </form>
                        </div>
                );
        }
});

export default Todolist;
